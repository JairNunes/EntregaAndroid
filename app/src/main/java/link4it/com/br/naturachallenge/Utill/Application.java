package link4it.com.br.naturachallenge.Utill;

import android.os.Environment;

import java.io.File;

import link4it.com.br.naturachallenge.R;
import support.api.sqlite.SQLiteDataBaseHelper;
import support.api.utils.LogTrace;
import support.api.utils.SupportApplication;


public class Application extends SupportApplication {

    static File sdcard = Environment.getExternalStorageDirectory();
    public final static File SDDIRETORIO = new File(sdcard.getAbsolutePath() + "/Natura");


    @Override
    public void onCreate() {

        super.onCreate();

        int version;

        try {

            //Cria banco de dados
            version = this.getResources().getInteger(R.integer.versaoDB);
            //  SDDIRETORIO.getAbsolutePath()+"/"+
            SQLiteDataBaseHelper.criaDB(this, SDDIRETORIO.getAbsolutePath() + "/" + this.getString(R.string.db_name), version);
            //SQLiteDataBaseHelper.criaDB(this, this.getString(R.string.db_name), version);

            //Seta cor padrão da aplicação
//            SupportBase.setDefaultColor(ContextCompat.getColor(this, R.color.toolbar_background));

            //Inicializa usuaio


        } catch (Exception err) {
        }
    }

    public abstract static class Config {


    }
}

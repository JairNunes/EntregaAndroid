package link4it.com.br.naturachallenge;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;




import java.util.List;

import link4it.com.br.naturachallenge.Fragment.CadastroFragment;
import link4it.com.br.naturachallenge.Model.Usuario;
import link4it.com.br.naturachallenge.Services.LoginService;
import link4it.com.br.naturachallenge.Services.ServiceGenerator;
import link4it.com.br.naturachallenge.Task.TaskLogin;
import link4it.com.br.naturachallenge.Utill.Support;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import support.api.utils.Alerta;

import static link4it.com.br.naturachallenge.Utill.Support.usuario;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnLogin;
    private EditText edtUsuario;
    private EditText edtSenha;
    private TaskLogin taskLogin;
    private List<Usuario> userList;
    ProgressDialog progress;
    private Activity mActivity;
    private Fragment mCurrentFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        inicalizaControles();

    }

    private void inicalizaControles() {
        mActivity = this;
        btnLogin = (Button) this.findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);
        edtUsuario = (EditText) this.findViewById(R.id.edtUsuario);
        edtSenha = (EditText) this.findViewById(R.id.edtSenha);


    }

    @Override
    public void onClick(View v) {
        try {

            progress = new ProgressDialog(LoginActivity.this);
            progress.setTitle("enviando...");
            progress.show();


            Usuario usuario = new Usuario();
            usuario.setUsuario(edtUsuario.getText().toString());
            usuario.setSenha(edtSenha.getText().toString());
            //chama o retrofit para fazer a requisição na api rest
            retrofitConverter(usuario);



        } catch (Exception e) {
            Alerta.show(mActivity,"Erro", e.getMessage());
            e.printStackTrace();
        }

    }

    public void retrofitConverter(Usuario usuario) {

        LoginService service = ServiceGenerator.createService(LoginService.class);

        service.login(usuario).enqueue(new Callback<Usuario>() {
        @Override
            public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                int statusCode = response.code();
                if(statusCode == 200) {
                    // Guardando os dados do usuario
                    Usuario u = response.body();
                    Support.usuario.setCodigo(u.getCodigo());
                    Support.usuario.setUsuario(u.getUsuario());
                    Support.usuario.setTipoUsuario("ff");
                    Support.usuario.setTipoLogin(u.getTipoLogin());

                    if (Support.usuario.getTipoUsuario() == "") {

                        Intent i = new Intent(mActivity, MenuActivity.class);
                        mActivity.startActivity(i);
                        mActivity.finish();

                    } else {

                        Intent i = new Intent(mActivity, MenuActivityCliente.class);
                        mActivity.startActivity(i);
                        mActivity.finish();
                    }

                } else {
                    Alerta.show(mActivity,"Response", "Usuário e senha inválidos");
                }

                progress.dismiss();
            }

            @Override
            public void onFailure(Call<Usuario> call, Throwable t) {

                t.printStackTrace();
            }

        });
    }


    public void cadastro(View view) {

        android.app.Fragment fragment = new CadastroFragment();
        FragmentManager fragmentManager = ((Activity) mActivity).getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.frame, fragment).commit();
    }
}

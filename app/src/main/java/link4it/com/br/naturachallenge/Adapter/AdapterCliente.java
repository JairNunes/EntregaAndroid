package link4it.com.br.naturachallenge.Adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import link4it.com.br.naturachallenge.Model.Cliente;
import link4it.com.br.naturachallenge.Model.Produto;
import link4it.com.br.naturachallenge.R;


public class AdapterCliente extends BaseAdapter {

    private Context mContext;
    private ArrayList<Cliente> clienteArrayList;

    public AdapterCliente(Context context, ArrayList<Cliente> clienteArrayList) {
        this.mContext = context;
        this.clienteArrayList = clienteArrayList;
    }

    @Override
    public int getCount() {
        return clienteArrayList.size();
    }

    @Override
    public Cliente getItem(int position) {
        return clienteArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        ViewHolder viewHolder;

        Cliente cliente;

        try {


            cliente = this.getItem(position);

            if (convertView == null) {
                convertView = View.inflate(mContext, R.layout.lst_cliente, null);

                viewHolder = new AdapterCliente.ViewHolder();
                viewHolder.lblNomeCliente = (TextView) convertView.findViewById(R.id.lblNomeCliente);
                viewHolder.lblEnderecoCliente = (TextView) convertView.findViewById(R.id.lblEnderecoCliente);
                viewHolder.lblNumeroCPF = (TextView) convertView.findViewById(R.id.lblNumeroCPF);

                convertView.setTag(viewHolder);

            } else {
                viewHolder = (AdapterCliente.ViewHolder) convertView.getTag();
            }

            viewHolder.lblEnderecoCliente.setText("Endereço : "+cliente.getEndereco());
            viewHolder.lblNomeCliente.setText("Nome : "+cliente.getNome());
            viewHolder.lblNumeroCPF.setText("CPF : "+cliente.getCpf().toString());


        } catch (Exception err) {

            err.printStackTrace();
        }


        return convertView;

    }


    private class ViewHolder {
        public TextView lblNomeCliente;
        public TextView lblEnderecoCliente;
        public TextView lblNumeroCPF;

    }
}

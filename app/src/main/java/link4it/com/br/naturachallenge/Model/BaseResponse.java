package link4it.com.br.naturachallenge.Model;

/**
 * Created by jair on 8/27/17.
 */


public abstract class BaseResponse {

    private String error = null;

    public String getError() {
        return error;
    }
}
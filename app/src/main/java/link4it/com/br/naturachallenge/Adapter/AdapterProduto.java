package link4it.com.br.naturachallenge.Adapter;


import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.util.List;

import link4it.com.br.naturachallenge.LoginActivity;
import link4it.com.br.naturachallenge.Model.Produto;
import link4it.com.br.naturachallenge.Model.Serializados.ProdutoClick;
import link4it.com.br.naturachallenge.R;
import link4it.com.br.naturachallenge.Utill.Requester;
import link4it.com.br.naturachallenge.Utill.Support;


/**
 * Created by jair on 19/05/17.
 */

public class AdapterProduto extends RecyclerView.Adapter<AdapterProduto.MyViewHolder> {

    private Context mContext;
    private List<Produto> produtoList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, count;
        public ImageView thumbnail, overflow;
        public CardView card_view;


        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            card_view = (CardView) view.findViewById(R.id.card_view);


        }
    }


    public AdapterProduto(Context mContext, List<Produto> produtoList) {
        this.mContext = mContext;
        this.produtoList = produtoList;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.produto_card, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {




        final Produto produto = produtoList.get(position);
        holder.title.setText(produto.getNome());
        String encdoedDataString = produto.getFoto().replace("data:image/jpeg;base64,", "");

        byte[] imageAsBytes = Base64.decode(encdoedDataString.getBytes(), 0);

        holder.thumbnail.setImageBitmap(BitmapFactory.decodeByteArray(
                       imageAsBytes, 0, imageAsBytes.length));
        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Requester req = new Requester(mContext,Requester.Tipo.CLICKS, null);
                ProdutoClick produtoClick = new ProdutoClick(Support.usuario.getCodigo(),produto.getCodigo());
                Gson gson = new Gson();
                req.requestGenerico(gson.toJson(produtoClick));
            }
        });


    }



    @Override
    public int getItemCount() {
        return produtoList.size();
    }
}

package link4it.com.br.naturachallenge.Task;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;

import org.json.JSONObject;

import link4it.com.br.naturachallenge.MenuActivity;
import link4it.com.br.naturachallenge.Model.Usuario;
import link4it.com.br.naturachallenge.Utill.Support;
import support.api.http.HTTP;
import support.api.http.Metodo;
import support.api.utils.Alerta;
import support.api.utils.Loading;

/**
 * Created by jair on 12/08/17.
 */

public class TaskLogin extends AsyncTask<Boolean, Boolean, Boolean> {


    private Usuario usuario;
    private JSONObject jsonObject;
    private HTTP http;
    private String url;
    private Activity mContext;
    private String response = "ezhomepi.dlinkddns.com:1025/Natura/rest/usuario/";

    public TaskLogin(Activity mContext,Usuario usuario) {
        this.usuario = usuario;
        this.mContext = mContext;
    }

    @Override
    protected void onPreExecute() {
        Loading.show(mContext);

    }

    @Override
    protected Boolean doInBackground(Boolean... booleen) {
        try {


            http = new HTTP(mContext, url);
            http.connect(usuario.getUsuario());
            http.setMetodo(Metodo.GET);
            response = http.getRetorno();
            jsonObject = new JSONObject(response);
            Support.usuario.setUsuario(jsonObject.getString(""));


        } catch (Exception e) {
            e.printStackTrace();
            Loading.hide();
            return false;
        }
        return true;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        Loading.hide();
        if(aBoolean){
            mContext.startActivity(new Intent(mContext,MenuActivity.class));
        }
        else{
            Alerta.show(mContext,"Atenção","Login ou senha inválidos!");
        }
    }
}

package link4it.com.br.naturachallenge.Fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import link4it.com.br.naturachallenge.R;



public class CadastroFragment extends Fragment implements View.OnClickListener {

    private Button btnSalvarCadastro;
    private Button btnCancelarCadastro;


    public CadastroFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_cadastro0, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        btnSalvarCadastro = (Button) this.getView().findViewById(R.id.btnSalvarCadastro);
        btnCancelarCadastro = (Button) this.getView().findViewById(R.id.btnCancelarCadastro);
        btnSalvarCadastro.setOnClickListener(this);
        btnSalvarCadastro.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if(v == btnCancelarCadastro){

        }else if (v == btnSalvarCadastro){

        }

    }
}

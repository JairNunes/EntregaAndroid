package link4it.com.br.naturachallenge.Model;

import java.util.ArrayList;
import java.util.List;



public class Cidade {
	

	private int codigo;
	
	private String nome;

	private Estado estado;
	
	private List<Bairro> bairros;
	
	public void adicionarBairro(Bairro bairro){
		bairro.setCidade(this);
		bairros.add(bairro);
		this.setBairros(bairros);
	}
	
	public Cidade() {
		super();
		// TODO Auto-generated constructor stub
		bairros = new ArrayList<Bairro>();
	}
	
	

	public Cidade(String nome, Estado estado, List<Bairro> bairros) {
		super();
		this.nome = nome;
		this.estado = estado;
		this.bairros = bairros;bairros = new ArrayList<Bairro>();
		
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public List<Bairro> getBairros() {
		return bairros;
	}

	public void setBairros(List<Bairro> bairros) {
		this.bairros = bairros;
	}

}

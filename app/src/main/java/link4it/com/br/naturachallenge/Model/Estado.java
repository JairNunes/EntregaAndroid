package link4it.com.br.naturachallenge.Model;

import java.util.ArrayList;
import java.util.List;


/**
 * 
 * @author Fernando C. Castro
 * @since 29/04/2017
 * 
 * Classe que representa um Estado. Ex.: S�o Paulo, Rio de Janeiro, etc.
 */


public class Estado {
	

	private int codigo;
	
	private String nome;

	private Pais pais;
	

	private List<Cidade> cidades = new ArrayList<Cidade>();
	
	public void adicionarCidade(Cidade cidade){
		cidade.setEstado(this);
		cidades.add(cidade);
		this.setCidades(cidades);
	}
	
	public Estado() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Estado(String nome, Pais pais) {
		super();
		this.nome = nome;
		this.pais = pais;
	}

	public Estado(String nome) {
		super();
		this.nome = nome;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Pais getPais() {
		return pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}

	public List<Cidade> getCidades() {
		return cidades;
	}

	public void setCidades(List<Cidade> cidades) {
		this.cidades = cidades;
	}

}

package link4it.com.br.naturachallenge.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import link4it.com.br.naturachallenge.Adapter.AdapterCarrinho;
import link4it.com.br.naturachallenge.Adapter.AdapterCliente;
import link4it.com.br.naturachallenge.Model.Cliente;
import link4it.com.br.naturachallenge.Model.Produto;
import link4it.com.br.naturachallenge.Model.ProdutoCarrinho;
import link4it.com.br.naturachallenge.R;



public class CarrinhoFragment extends Fragment {

    private List<Produto> produtoList;
    private ProdutoCarrinho carrinho;
    private ListView lvCarrinho;
    private AdapterCarrinho adapter;
    private Context mContext;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_carrinho, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mContext = this.getActivity();
        carregaDados();
        inicializaControles();


    }


    private void inicializaControles(){
        lvCarrinho = (ListView) this.getActivity().findViewById(R.id.lvCarrinho);
        adapter = new AdapterCarrinho(this.getActivity(),carrinho);
        lvCarrinho.setAdapter(adapter);




    }

    private void carregaDados(){

        produtoList = new ArrayList<Produto>();
        Produto  p = new Produto("Batton",0);

        carrinho = new ProdutoCarrinho(1,produtoList,0,0,0);





    }


}

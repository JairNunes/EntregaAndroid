package link4it.com.br.naturachallenge.Model;

import java.util.ArrayList;
import java.util.List;


/**
 * @author Fernando C. Castro
 * @since 29/04/2017
 * <p>
 * Classe que representa um Bairro. Ex.: Vila Mariana, Sa�de, etc.
 */


public class Bairro {

    private int codigo;
    private String nome;


    private Cidade cidade;

    private List<Logradouro> logradouros = new ArrayList<Logradouro>();

    public void adicionarLogradouro(Logradouro logradouro) {
        logradouro.setBairro(this);
        logradouros.add(logradouro);
        this.setLogradouros(logradouros);
    }

    public Bairro() {
        super();
        // TODO Auto-generated constructor stub
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public List<Logradouro> getLogradouros() {
        return logradouros;
    }

    public void setLogradouros(List<Logradouro> logradouros) {
        this.logradouros = logradouros;
    }

}

package link4it.com.br.naturachallenge.Model;

import java.util.Calendar;

import link4it.com.br.naturachallenge.Enum.StatusPedido;

/**
 * 
 * @author Fernando C. Castro
 * @since 29/04/2017
 */
public class Pedido {
	private int codigo;
	private Calendar dataPedido;
	private StatusPedido status;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public Calendar getDataPedido() {
		return dataPedido;
	}

	public void setDataPedido(Calendar dataPedido) {
		this.dataPedido = dataPedido;
	}

	public StatusPedido getStatus() {
		return status;
	}

	public void setStatus(StatusPedido status) {
		this.status = status;
	}
}

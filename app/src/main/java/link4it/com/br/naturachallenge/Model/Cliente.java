package link4it.com.br.naturachallenge.Model;

import java.util.Calendar;

import link4it.com.br.naturachallenge.Enum.Sexo;

/**
 * 
 * @author Fernando C. Castro
 * @since 29/04/2017
 * 
 * Classe que representa um cliente da consultora natura.
 */
public class Cliente {
	private int id;
	private Calendar dataNascimento;
	private Sexo sexo;
	private String nome;
	private String cpf;
	private String endereco;


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public Cliente() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Cliente(int id, String nome, String cpf, String endereco) {
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Calendar getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Calendar dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

}

package link4it.com.br.naturachallenge.Model;

import android.util.Base64;

/**
 * 
 * @author Fernando C. Castro
 * @since 29/04/2017
 * 
 * Representa um produto natura.
 */
public class Produto {
	private double valor;
	private int codigo;
	private String nome;
	private String descricao;
	private String duracaoEstimada;
	private int quantidadeEmEstoque;
	private int cover;
	private byte[] thumbnail;

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    private String foto;

	public void setCover(int cover) {
		this.cover = cover;
	}

	public void setThumbnail(byte[] thumbnail) {
		this.thumbnail = thumbnail;
	}

	public Produto(String nome, int cover) {
		this.nome = nome;
		this.cover = cover;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getDuracaoEstimada() {
		return duracaoEstimada;
	}

	public void setDuracaoEstimada(String duracaoEstimada) {
		this.duracaoEstimada = duracaoEstimada;
	}

	public int getQuantidadeEmEstoque() {
		return quantidadeEmEstoque;
	}

	public void setQuantidadeEmEstoque(int quantidadeEmEstoque) {
		this.quantidadeEmEstoque = quantidadeEmEstoque;
	}

	public int getCover() {
		return cover;
	}

	public byte[] getThumbnail() {
        return thumbnail;

    }
}

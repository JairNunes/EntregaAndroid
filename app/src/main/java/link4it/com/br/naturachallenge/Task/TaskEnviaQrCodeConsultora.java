package link4it.com.br.naturachallenge.Task;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONObject;

import link4it.com.br.naturachallenge.Model.Consultora;
import support.api.http.HTTP;
import support.api.http.Metodo;
import support.api.utils.Alerta;
import support.api.utils.Loading;

/**
 * Created by user on 29/08/17.
 */

public class TaskEnviaQrCodeConsultora extends AsyncTask<Boolean, Boolean, Boolean> {


    private Consultora consultora;
    private JSONObject jsonObject;
    private HTTP http;
    private String url;
    private Activity mContext;
    private String response = "ezhomepi.dlinkddns.com:1025/Natura/rest/cadastro/";

    public TaskEnviaQrCodeConsultora(Activity mContext, Consultora consultora) {
        this.consultora = consultora;
        this.mContext = mContext;
    }

    @Override
    protected void onPreExecute() {
        Loading.show(mContext);

    }

    @Override
    protected Boolean doInBackground(Boolean... booleen) {
        try {
            //COMENTAR DEPOIS A LINHA A BAIXO

            String param = new Gson().toJson(consultora);
            http = new HTTP(mContext, url);

            http.connect(param);
            Log.i("JSON PARAM",param);
            http.setMetodo(Metodo.GET);
            response = http.getRetorno();
            jsonObject = new JSONObject(response);



        } catch (Exception e) {
            e.printStackTrace();
            Loading.hide();
            return false;
        }
        return true;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        Loading.hide();
        if(aBoolean){
            Alerta.show(mContext,"Sucesso!","Cadastro realizado com sucesso");
        }
        else{
            Alerta.show(mContext,"Atenção","Login ou senha inválidos!");
        }
    }
}
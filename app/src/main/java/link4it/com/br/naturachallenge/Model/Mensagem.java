package link4it.com.br.naturachallenge.Model;

import java.util.Calendar;

import link4it.com.br.naturachallenge.Enum.StatusMensagem;

/**
 * 
 * @author Fernando C. Castro
 * @since 29/04/2017
 * 
 * Classe de mensagem entre Consultora e Cliente. Simples chat, como se fosse um email direto entre elas. 
 */
public class Mensagem {
	
	private int codigo;
	private String nome;
	private String mensagem;
	private Calendar data;
	private StatusMensagem status;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public Calendar getData() {
		return data;
	}

	public void setData(Calendar data) {
		this.data = data;
	}

	public StatusMensagem getStatus() {
		return status;
	}

	public void setStatus(StatusMensagem status) {
		this.status = status;
	}
}

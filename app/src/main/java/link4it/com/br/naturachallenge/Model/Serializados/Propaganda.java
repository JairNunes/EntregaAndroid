package link4it.com.br.naturachallenge.Model.Serializados;

import com.google.gson.annotations.SerializedName;

import link4it.com.br.naturachallenge.Model.Consultora;

/**
 * Created by Rafael Macedo on 02/10/2017.
 */

public class Propaganda {
    @SerializedName(value = "Titulo")
    private String titulo;
    @SerializedName(value = "Descricao")
    private String descrica;
    @SerializedName(value = "Foto")
    private String imagemBase64;
    @SerializedName(value = "CaminhoFoto")
    private String caminhoFoto;
    @SerializedName(value = "Consultora")
    private Consultora consultora;


    public Propaganda(String titulo, String descrica, String imagemBase64, String caminhoFoto, Consultora consultora) {
        this.titulo = titulo;
        this.descrica = descrica;
        this.imagemBase64 = imagemBase64;
        this.caminhoFoto = caminhoFoto;
        this.consultora = consultora;
    }

    public Propaganda() {
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescrica() {
        return descrica;
    }

    public void setDescrica(String descrica) {
        this.descrica = descrica;
    }

    public String getImagemBase64() {
        return imagemBase64;
    }

    public void setImagemBase64(String imagemBase64) {
        this.imagemBase64 = imagemBase64;
    }

    public String getCaminhoFoto() {
        return caminhoFoto;
    }

    public void setCaminhoFoto(String caminhoFoto) {
        this.caminhoFoto = caminhoFoto;
    }

    public Consultora getConsultora() {
        return consultora;
    }

    public void setConsultora(Consultora consultora) {
        this.consultora = consultora;
    }
}






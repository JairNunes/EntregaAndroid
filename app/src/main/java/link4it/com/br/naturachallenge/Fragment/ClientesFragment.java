package link4it.com.br.naturachallenge.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import link4it.com.br.naturachallenge.Adapter.AdapterCliente;
import link4it.com.br.naturachallenge.Adapter.AdapterProduto;
import link4it.com.br.naturachallenge.MenuActivity;
import link4it.com.br.naturachallenge.Model.Cliente;
import link4it.com.br.naturachallenge.Model.Produto;
import link4it.com.br.naturachallenge.R;


public class ClientesFragment extends Fragment {

    private ArrayList<Cliente> clienteArrayList;
    ListView lvClientes;
    AdapterCliente adapter;
    MenuActivity mContext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_clientes, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {

            mContext = (MenuActivity) this.getActivity();
            carregaDados();
            inicializaControles();


        } catch (Exception err) {
            err.printStackTrace();

        }
    }

    private void inicializaControles() {
        lvClientes = (ListView) mContext.findViewById(R.id.lvClientes);
        adapter = new AdapterCliente(mContext, clienteArrayList);
        lvClientes.setAdapter(adapter);


    }

    private void carregaDados() {

        clienteArrayList = new ArrayList<Cliente>();
        clienteArrayList.add(new Cliente(1, "Andressa", "222.222.222", "Rua 25"));
        clienteArrayList.add(new Cliente(1, "Lucas", "333.333.333", "Rua Rua Rua"));
        clienteArrayList.add(new Cliente(1, "Rafael", "4444.4444.4444", "ASD RuDa Rua"));


    }

}


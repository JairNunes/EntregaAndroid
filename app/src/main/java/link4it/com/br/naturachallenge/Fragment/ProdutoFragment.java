package link4it.com.br.naturachallenge.Fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import link4it.com.br.naturachallenge.Services.ServiceGenerator;

import java.util.ArrayList;
import java.util.List;

import link4it.com.br.naturachallenge.Adapter.AdapterProduto;
import link4it.com.br.naturachallenge.Model.Produto;
import link4it.com.br.naturachallenge.R;
import link4it.com.br.naturachallenge.Services.ProdutoService;

import link4it.com.br.naturachallenge.Utill.Support;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import support.api.utils.Alerta;


public class ProdutoFragment extends Fragment {

    private RecyclerView recyclerView;
    private AdapterProduto adapter;
    private List<Produto> produtoList;
    private Activity mContext;
    ProgressDialog progress;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_produto, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            mContext = this.getActivity();
            progress = new ProgressDialog(mContext);
            progress.setTitle("Obtendo Produtos...");
            progress.show();

            recyclerView = (RecyclerView) getActivity(). findViewById(R.id.recycler_view);
            produtoList = new ArrayList<>();
            adapter = new AdapterProduto(this.getActivity(), produtoList);

            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this.getActivity(), 2);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(adapter);

            retrofitConverter();

            progress.dismiss();

        }
        catch (Exception err) {

            err.printStackTrace();

        }
    }


    public void retrofitConverter() {


        ProdutoService service = ServiceGenerator.createService(ProdutoService.class);


        service.produtos().enqueue(new Callback<List<Produto>>() {
            @Override
            public void onResponse(Call<List<Produto>> call, Response<List<Produto>> response) {
                int statusCode = response.code();
                if(statusCode == 200) {
                    // Guardando os dados do produto
                    produtoList.addAll(response.body());

                }

                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Produto>> call, Throwable t) {
                Alerta.show(mContext,"Response",  t.getMessage() + "");

                t.printStackTrace();
            }

        });
    }


    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

}

package link4it.com.br.naturachallenge.Model;

import java.util.ArrayList;
import java.util.List;

/** 
 * 
 * @author Fernando C. Castro
 * @since 29/04/2017
 * 
 * Classe que representa um logradouro. Ex.: Lins de vasconcelos
 */

public class Logradouro {

	private String cep;
	

	private String logradouro;



	private Bairro bairro;
	

	private TipoLogradouro tipoLogradouro;

	private List<Endereco> enderecos = new ArrayList<Endereco>();
	
	public void adicionarEndereco(Endereco endereco){
		endereco.setLogradouro(this);
		enderecos.add(endereco);
		this.setEnderecos(enderecos);
	}
	
	
	public Logradouro() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Logradouro(String cep, String logradouro, Bairro bairro, TipoLogradouro tipoLogradouro,
			List<Endereco> enderecos) {
		super();
		this.cep = cep;
		this.logradouro = logradouro;
		this.bairro = bairro;
		this.tipoLogradouro = tipoLogradouro;
		this.enderecos = enderecos;
	}


	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public Bairro getBairro() {
		return bairro;
	}

	public void setBairro(Bairro bairro) {
		this.bairro = bairro;
	}

	public List<Endereco> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}


	public TipoLogradouro getTipoLogradouro() {
		return tipoLogradouro;
	}


	public void setTipoLogradouro(TipoLogradouro tipoLogradouro) {
		this.tipoLogradouro = tipoLogradouro;
	}

}

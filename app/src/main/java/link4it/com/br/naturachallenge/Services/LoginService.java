package link4it.com.br.naturachallenge.Services;

import link4it.com.br.naturachallenge.Model.Usuario;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by jair on 8/23/17.
 */

public interface LoginService {
    @POST("/logon/login")
    Call<Usuario> login(@Body Usuario usuario);
}
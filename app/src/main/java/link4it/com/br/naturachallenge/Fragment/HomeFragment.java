package link4it.com.br.naturachallenge.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import link4it.com.br.naturachallenge.MenuActivity;
import link4it.com.br.naturachallenge.R;



public class HomeFragment extends Fragment implements BottomNavigationView.OnNavigationItemSelectedListener{

    MenuActivity mContext;
    private Fragment mCurrentFragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        BottomNavigationView navigation = (BottomNavigationView) getActivity().findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {


        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();

        switch (menuItem.getItemId()) {
            case R.id.navegation_meusPedidos:
                if (!(mCurrentFragment instanceof HomeFragment)) {
                    mCurrentFragment = new HomeFragment();
                    transaction.replace(R.id.frame, mCurrentFragment);
                    transaction.commit();
                }
                return true;
            case R.id.navegation_avisos:
                if (!(mCurrentFragment instanceof HomeFragment)) {
                    mCurrentFragment = new HomeFragment();
                    transaction.replace(R.id.frame, mCurrentFragment);
                    transaction.commit();
                }
                return true;
            case R.id.navegation_relatorio:
                if (!(mCurrentFragment instanceof RelatorioFragment)) {
                    mCurrentFragment = new RelatorioFragment();
                    transaction.replace(R.id.frame, mCurrentFragment);
                    transaction.commit();
                }
                return true;
        }
        return false;

    }



}
package link4it.com.br.naturachallenge.Model;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import link4it.com.br.naturachallenge.Enum.TipoLogin;

/**
 * 
 * @author Fernando C. Castro
 * @since 27/04/2017
 * 
 * Classe genérica de usuário de software. Um usuário pode ser uma consultora ou um cliente.
 */

public class Usuario {


	private int codigo;


	private String usuario;


	private String senha;

	private String jwt;


	private String idFacebook;


	private String tipoUsuario;

	private TipoLogin tipoLogin;

	public Usuario() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Usuario(String login, String senha, String idFacebook) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		super();
		this.usuario = login;
		this.senha = senha;
		this.idFacebook = idFacebook;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha){
		this.senha = senha;
	}

	public String getIdFacebook() {
		return idFacebook;
	}

	public void setIdFacebook(String idFacebook) {
		this.idFacebook = idFacebook;
	}

	public String getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public TipoLogin getTipoLogin() {
		return tipoLogin;
	}

	public void setTipoLogin(TipoLogin tipoLogin) {
		this.tipoLogin = tipoLogin;
	}
}

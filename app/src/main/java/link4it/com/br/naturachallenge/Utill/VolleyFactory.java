package link4it.com.br.naturachallenge.Utill;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Rafael Macedo on 01/10/2017.
 */

public class VolleyFactory {
    private static VolleyFactory mySingleTon;
    private RequestQueue requestQueue;
    private static Context context;

    private VolleyFactory(Context context) {
        this.context = context;
        this.requestQueue = getRequestQueue();

    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return requestQueue;
    }

    public static synchronized VolleyFactory getInstance(Context context) {
        if (mySingleTon == null) {
            mySingleTon = new VolleyFactory(context);
        }
        return mySingleTon;
    }

    public <T> void addToRequestQue(Request<T> request) {
        requestQueue.add(request);

    }
}

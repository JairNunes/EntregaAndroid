package link4it.com.br.naturachallenge.Adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import link4it.com.br.naturachallenge.Model.Cliente;
import link4it.com.br.naturachallenge.Model.Produto;
import link4it.com.br.naturachallenge.Model.ProdutoCarrinho;
import link4it.com.br.naturachallenge.R;


public class AdapterCarrinho extends BaseAdapter {

    private Context mContext;
    private ProdutoCarrinho carrinho;

    public AdapterCarrinho(Context context, ProdutoCarrinho carrinho) {
        this.mContext = context;
        this.carrinho = carrinho;

    }

    @Override
    public int getCount() {
        return carrinho.getProdutoList().size();
    }

    @Override
    public Produto getItem(int position) {
        return carrinho.getProdutoList().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        ViewHolder viewHolder;

        Produto produtoCarrinho;

        try {


            produtoCarrinho = this.getItem(position);

            if (convertView == null) {
                convertView = View.inflate(mContext, R.layout.lst_produto_carrinho, null);

                viewHolder = new AdapterCarrinho.ViewHolder();
                viewHolder.lblIdProdutoCarrinho = (TextView) convertView.findViewById(R.id.lblIdProdutoCarrinho);
                viewHolder.lblQuantidadeProdutoCarrinho = (TextView) convertView.findViewById(R.id.lblQuantidadeProdutoCarrinho);
                viewHolder.lblValorUndProdutoCarrinho = (TextView) convertView.findViewById(R.id.lblValorUndProdutoCarrinho);
                viewHolder.lblTotalCarrinho = (TextView) convertView.findViewById(R.id.lblTotalCarrinho);

                convertView.setTag(viewHolder);

            } else {
                viewHolder = (AdapterCarrinho.ViewHolder) convertView.getTag();
            }

            viewHolder.lblIdProdutoCarrinho.setText("ID:" + 1);
            viewHolder.lblQuantidadeProdutoCarrinho.setText("Qtd. 1");
            viewHolder.lblTotalCarrinho.setText("TOTAL: "+"R$ " + String.format("%.2f", carrinho.getValorTotal()));
            viewHolder.lblValorUndProdutoCarrinho.setText("R$ "+String.format("%.2f", produtoCarrinho.getValor()));


        } catch (Exception err) {

            err.printStackTrace();
        }


        return convertView;

    }


    private class ViewHolder {
        public TextView lblIdProdutoCarrinho;
        public TextView lblValorUndProdutoCarrinho;
        public TextView lblQuantidadeProdutoCarrinho;
        public TextView lblTotalCarrinho;

    }
}

package link4it.com.br.naturachallenge.Model;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Fernando C. Castro
 * @since 29/04/2017
 * 
 * Classe que representa um Pa�s. Ex:. Brasil, Col�mbia, etc.
 */


public class Pais {

	private int codigo;
	

	private String nome;
	

	private List<Estado> estados;
	
	public void adicionarEstado(Estado estado){
		estado.setPais(this);
		estados.add(estado);
		this.setEstados(estados);
	}

	public Pais() {
		super();
		estados = new ArrayList<Estado>();
		// TODO Auto-generated constructor stub
	}
	

	public Pais(String nome) {
		super();
		this.nome = nome;
		estados = new ArrayList<Estado>();
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Estado> getEstados() {
		return estados;
	}

	public void setEstados(List<Estado> estados) {
		this.estados = estados;
	}
}

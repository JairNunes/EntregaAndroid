package link4it.com.br.naturachallenge;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import link4it.com.br.naturachallenge.Model.Usuario;
import link4it.com.br.naturachallenge.Utill.Support;


public class MainActivity extends AppCompatActivity implements Runnable, Animation.AnimationListener {

    ImageView imgLogo;
    Animation animTogether;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {

            imgLogo = (ImageView) this.findViewById(R.id.imgLogoPrincipal);

            animTogether = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blink);

            animTogether.setAnimationListener(this);
            imgLogo.startAnimation(animTogether);


        } catch (Exception err) {

        }


    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

        new Handler().postDelayed(this, 1000);

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    @Override
    public void run() {
        if (Support.usuario == null) {
            Support.usuario =  new Usuario();
            Intent i = new Intent(this, LoginActivity.class);
            this.startActivity(i);
            this.finish();
        } else {

            this.startActivity(new Intent(this, MenuActivity.class));
            this.finish();
        }
    }
}

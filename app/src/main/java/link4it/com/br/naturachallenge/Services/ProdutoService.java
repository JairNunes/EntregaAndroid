package link4it.com.br.naturachallenge.Services;

import java.util.List;

import link4it.com.br.naturachallenge.Model.Produto;
import link4it.com.br.naturachallenge.Model.Usuario;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by jair on 8/23/17.
 */

public interface ProdutoService {
    @GET("/produto/listar")
    Call<List<Produto>> produtos();
}
package link4it.com.br.naturachallenge.Model;

import link4it.com.br.naturachallenge.Enum.StatusCampanha;

/**
 * 
 * @author Fernando C. Castro
 * @since 29/04/2017
 * 
 * Classe de promo��es. Representa uma campanha da consultora.
 */
public class Campanha {

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public byte[] getImagem() {
		return imagem;
	}

	public void setImagem(byte[] imagem) {
		this.imagem = imagem;
	}

	public StatusCampanha getStatus() {
		return status;
	}

	public void setStatus(StatusCampanha status) {
		this.status = status;
	}

	private String titulo;
	private String descricao;
	private byte[] imagem;
	private StatusCampanha status;
}

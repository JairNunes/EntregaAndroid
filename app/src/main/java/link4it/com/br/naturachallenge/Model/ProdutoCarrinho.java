package link4it.com.br.naturachallenge.Model;

import java.util.List;

/**
 * Created by user on 18/05/17.
 */

public class ProdutoCarrinho {
    private int idCarrinho;
    private List<Produto> produtoList;
    private int quantidade;
    private double valorUnidade;
    private double valorTotal;

    public int getIdCarrinho() {
        return idCarrinho;
    }

    public void setIdCarrinho(int idCarrinho) {
        this.idCarrinho = idCarrinho;
    }

    public List<Produto> getProdutoList() {
        return produtoList;
    }

    public void setProdutoList(List<Produto> produtoList) {
        this.produtoList = produtoList;
    }

    public int getQuantidade() {
        return this.quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public double getValorUnidade() {
        return valorUnidade;
    }

    public void setValorUnidade(double valorUnidade) {
        this.valorUnidade = valorUnidade;
    }

    public double getValorTotal() {

        for (Produto p : produtoList
                ) {
            valorTotal =+ p.getValor();

            return valorTotal;

        }
        return valorTotal;
    }

    public void setValorTotal(double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public ProdutoCarrinho(int idCarrinho, List<Produto> produtoList, int quantidade, double valorUnidade, double valorTotal) {
        this.idCarrinho = idCarrinho;
        this.produtoList = produtoList;
        this.quantidade = quantidade;
        this.valorUnidade = valorUnidade;
        this.valorTotal = valorTotal;
    }
}

package link4it.com.br.naturachallenge.Model;

import java.util.ArrayList;
import java.util.List;


/**
 * 
 * @author Fernando C. Castro
 * @since 29/04/2017
 * 
 * Classe que representa um tipo de logradouro. Ex.: Rua, Avenida, Rodovia, etc.
 */


public class TipoLogradouro {
	

	private int codigo;
	

	private String descricao;


	private List<Logradouro> logradouros = new ArrayList<Logradouro>();
	
	public void adicionarLogradouro(Logradouro logradouro){
		logradouro.setTipoLogradouro(this);
		logradouros.add(logradouro);
		this.setLogradouros(logradouros);
	}
	
	public TipoLogradouro() {
		super();
	}
	

	public TipoLogradouro(String descricao, List<Logradouro> logradouros) {
		super();
		this.descricao = descricao;
		this.logradouros = logradouros;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<Logradouro> getLogradouros() {
		return logradouros;
	}

	public void setLogradouros(List<Logradouro> logradouros) {
		this.logradouros = logradouros;
	}

}

package link4it.com.br.naturachallenge.Model;

import com.google.gson.annotations.SerializedName;

/**
 * 
 * @author Fernando C. Castro
 * @since 29/04/2017
 * 
 *        Classe de consultoras natura.
 */
public class Consultora {
    @SerializedName("id")
	private int id;
    @SerializedName("nome")
	private String nome;
    @SerializedName("data_nascimento")
	private String dataNascimento;
    @SerializedName("sexo")
	private String sexo;

	public Consultora() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

}

package link4it.com.br.naturachallenge.Adapter;


import android.content.Context;
import android.graphics.PorterDuff;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import link4it.com.br.naturachallenge.Model.DrawerItem;
import link4it.com.br.naturachallenge.R;


public class AdapterDrawer extends BaseAdapter {

    private Context mContext;
    private ArrayList<DrawerItem> mArrDrawerItens;
    private int mDrawerSelecionado = 0;

    public AdapterDrawer(Context context, ArrayList<DrawerItem> arrDrawerItens) {
        this.mContext = context;
        this.mArrDrawerItens = arrDrawerItens;
    }

    @Override
    public int getCount() {
        return mArrDrawerItens.size();
    }

    @Override
    public Object getItem(int position) {
        return mArrDrawerItens.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        AdapterDrawer.ViewHolder viewHolder;
        DrawerItem drawerItem = (DrawerItem) this.getItem(position);
        int corTexto = 0;

        if (convertView == null) {

            convertView = View.inflate(mContext, R.layout.lst_draweritem, null);

            viewHolder = new ViewHolder();
            viewHolder.row = (RelativeLayout) convertView.findViewById(R.id.row);
            viewHolder.imgIcon = (ImageView) convertView.findViewById(R.id.imgIcon);
            viewHolder.lblTitulo = (TextView) convertView.findViewById(R.id.lblTitulo);
            viewHolder.lblNumero = (TextView) convertView.findViewById(R.id.lblNumero);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        //Verifica se esse é o drawer selecionado
        if (drawerItem.getTitulo() == mDrawerSelecionado) {
            corTexto = mContext.getResources().getColor(R.color.drawer_ativo_texto);
            viewHolder.row.setBackgroundColor(mContext.getResources().getColor(R.color.drawer_inativo_linha));
        } else {
            corTexto = mContext.getResources().getColor(R.color.drawer_inativo_texto);
            viewHolder.row.setBackgroundColor(mContext.getResources().getColor(R.color.drawer_inativo_linha));
        }

        //Seta cor
        viewHolder.imgIcon.setColorFilter(corTexto, PorterDuff.Mode.SRC_ATOP);
        viewHolder.lblTitulo.setTextColor(corTexto);
        viewHolder.lblNumero.setTextColor(corTexto);

        //Seta texto
        viewHolder.imgIcon.setImageResource(drawerItem.getIcone());
        viewHolder.lblTitulo.setText(drawerItem.getTitulo());

        //Verifica se exibe numero
        if (drawerItem.isExibeNumero()) {
            viewHolder.lblNumero.setVisibility(View.VISIBLE);
            viewHolder.lblNumero.setText(
                    String.valueOf(drawerItem.getNumero())
            );
            if (drawerItem.getBackgroudNumero() > 0)
                viewHolder.lblNumero.setBackgroundResource(drawerItem.getBackgroudNumero());
        } else
            viewHolder.lblNumero.setVisibility(View.GONE);

        return convertView;
    }

    public void notifyDataSetChanged(int drawerSelecionado) {
        mDrawerSelecionado = drawerSelecionado;
        super.notifyDataSetChanged();
    }

    private class ViewHolder {
        public RelativeLayout row;
        public ImageView imgIcon;
        public TextView lblTitulo;
        public TextView lblNumero;
    }
}

package link4it.com.br.naturachallenge.Utill;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

/**
 * Created by Rafael Macedo on 01/10/2017.
 */

public class Requester implements Response.Listener<String>, Response.ErrorListener {
    private Tipo tipoRequisicao;
    private String url;
    private Context mContext;
    private VolleyCallback callback;

    public Requester(Context mContext, Tipo tipoRequisicao, VolleyCallback callback) {
        this.tipoRequisicao = tipoRequisicao;
        this.mContext = mContext;
        this.callback = callback;
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(String response) {
        if (callback != null){
            callback.onSuccess(response);

        }

    }

    public void requestGenerico(final String request) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, geradorDeURL(tipoRequisicao), this, this) {
            @Override
            public byte[] getBody() throws AuthFailureError {

                return request.getBytes();
            }
        };

        VolleyFactory.getInstance(mContext).addToRequestQue(stringRequest);


    }

    public enum Tipo {
        CLICKS,CADASTRO,ASSOCIAR,BUSCA_USUARIO,BUSCA_PRODUTO,PROPAGANDA,ATUALIZA_PROPAGANDA
    }

    private String geradorDeURL(Tipo tipo) {
        if (tipo == Tipo.CLICKS) {
            StringBuilder sbQueryString = new StringBuilder();
            sbQueryString.append("http://labcastro-com.umbler.net/ProdutoInteresse/inserir");

            return sbQueryString.toString();
        } else if (tipo == Tipo.ASSOCIAR){

        }
        else if (tipo == Tipo.ASSOCIAR){

        }
        else if (tipo == Tipo.CADASTRO){
            return "http://labcastro-com.umbler.net/usuario/cadastro";

        }
        else if (tipo == Tipo.BUSCA_USUARIO){
            return "http://labcastro-com.umbler.net/usuario/buscarUsuario";

        }
        else if (tipo == Tipo.BUSCA_PRODUTO){

        }else if (tipo == Tipo.ASSOCIAR.PROPAGANDA){
            return "http://labcastro-com.umbler.net/propaganda/listar/24";
        }else if (tipo == Tipo.ATUALIZA_PROPAGANDA){
            return "http://labcastro-com.umbler.net/propaganda/atualizar";
        }
            return null;

    }
}

package link4it.com.br.naturachallenge.Model;


/**
 * 
 * @author Fernando C. Castro
 * @since 29/04/2016
 * 
 * Classe que representa um endere�o.
 */


public class Endereco {
	

	private int codigo;
	

	private Usuario usuario;
	

	private String complemento;

	private int numero;
	

	private Logradouro logradouro;

	public Endereco() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public Logradouro getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(Logradouro logradouro) {
		this.logradouro = logradouro;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
}

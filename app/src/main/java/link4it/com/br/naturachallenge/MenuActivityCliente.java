package link4it.com.br.naturachallenge;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import link4it.com.br.naturachallenge.Fragment.AvisoFragment;
import link4it.com.br.naturachallenge.Fragment.HomeFragment;
import link4it.com.br.naturachallenge.Fragment.MsgClienteConsultoraFragment;
import link4it.com.br.naturachallenge.Fragment.ProdutoFragment;

public class MenuActivityCliente extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private Fragment mCurrentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        try {
            this.abreTela(R.string.principal, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        try {

            if (id == R.id.nav_qr_code) {
                abreTela(R.string.qrCode, null);
            } else if (id == R.id.nav_produtos) {
                abreTela(R.string.Produtos, null);

            } else if (id == R.id.nav_avisos) {
                abreTela(R.string.aviso, null);
            } else if (id == R.id.nav_catalogo) {
                abreTela(R.string.Produtos, null);
            } else if (id == R.id.nav_home) {
                abreTela(R.string.principal, null);
            } else if (id == R.id.nav_mensagem) {
                abreTela(R.string.mensagem, null);
            } else if (id == R.id.nav_mkt_facebook) {

                abreTela(R.string.marketing, null);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void abreTela(int titulo, Bundle param) throws Exception {


        FragmentTransaction transaction = this.getSupportFragmentManager().beginTransaction();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(null);

        switch (titulo) {

            case R.string.principal:
                toolbar.setTitle(R.string.principal);
                if (!(mCurrentFragment instanceof HomeFragment)) {
                    mCurrentFragment = new HomeFragment();
                    transaction.replace(R.id.frame, mCurrentFragment);
                }
                break;

            case R.string.Produtos:
                toolbar.setTitle(R.string.Produtos);
                if (!(mCurrentFragment instanceof ProdutoFragment)) {
                    mCurrentFragment = new ProdutoFragment();
                    transaction.replace(R.id.frame, mCurrentFragment);


                }
                break;

//            case R.string.qrCode:
//                toolbar.setTitle(R.string.qrCode);
//                if (!(mCurrentFragment instanceof QrCodeFragment)) {
//                    mCurrentFragment = new QrCodeFragment();
//                    transaction.replace(R.id.frame, mCurrentFragment);
//
//                }
//                break;
            case R.string.aviso:
                toolbar.setTitle(R.string.mensagem);
                if (!(mCurrentFragment instanceof AvisoFragment)) {
                    mCurrentFragment = new AvisoFragment();
                    transaction.replace(R.id.frame, mCurrentFragment);

                }
                break;
            case R.string.mensagem:
                toolbar.setTitle(R.string.mensagem);
                if (!(mCurrentFragment instanceof MsgClienteConsultoraFragment)) {
                    mCurrentFragment = new MsgClienteConsultoraFragment();
                    transaction.replace(R.id.frame, mCurrentFragment);

                }
                break;
            case R.string.logout:
                Intent login = new Intent(this, LoginActivity.class);
                startActivity(login);
                this.finish();

                break;


        }
        if (param != null)
            mCurrentFragment.setArguments(param);

        transaction.commit();
    }


}

package link4it.com.br.naturachallenge.Utill;

/**
 * Created by Rafael Macedo on 01/10/2017.
 */

public interface VolleyCallback {
    public void onSuccess(String response);
}

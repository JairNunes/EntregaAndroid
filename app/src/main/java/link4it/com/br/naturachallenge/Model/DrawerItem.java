package link4it.com.br.naturachallenge.Model;

/**
 * Created by logonrm on 13/05/2017.
 */
    public class DrawerItem {
        private int icone;
        private int titulo;
        private int subtitulo;
        private boolean exibeNumero;
        private String numero;
        private int backgroudNumero = 0;

        public DrawerItem(int icone, int titulo){
            this.icone = icone;
            this.titulo = titulo;
            exibeNumero = false;
        }

        public DrawerItem(int icone, int titulo, String numero) {
            this.icone = icone;
            this.titulo = titulo;
            exibeNumero = true;
            this.numero = numero;
            backgroudNumero = 0;
        }

        public DrawerItem(int icone, int titulo, String numero, int background) {
            this.icone = icone;
            this.titulo = titulo;
            exibeNumero = true;
            this.numero = numero;
            backgroudNumero = background;
        }

        public int getIcone() {
            return icone;
        }

        public void setIcone(int icone) {
            this.icone = icone;
        }

        public int getTitulo() {
            return titulo;
        }

        public void setTitulo(int titulo) {
            this.titulo = titulo;
        }

        public int getSubtitulo() {
            return subtitulo;
        }

        public void setSubtitulo(int subtitulo) {
            this.subtitulo = subtitulo;
        }

        public boolean isExibeNumero() {
            return exibeNumero;
        }

        public void setExibeNumero(boolean exibeNumero) {
            this.exibeNumero = exibeNumero;
        }

        public String getNumero() {
            return numero;
        }

        public void setNumero(String numero) {
            this.numero = numero;
        }

        public int getBackgroudNumero() {
            return backgroudNumero;
        }

        public void setBackgroudNumero(int backgroudNumero) {
            this.backgroudNumero = backgroudNumero;
        }
    }


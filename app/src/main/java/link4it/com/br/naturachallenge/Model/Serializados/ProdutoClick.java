package link4it.com.br.naturachallenge.Model.Serializados;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Rafael Macedo on 01/10/2017.
 */

public class ProdutoClick {
    @SerializedName(value = "CodigoProduto")
    private int codigoProduto;
    @SerializedName(value = "CodigoUsuario")
    private int codigoUsuario;

    public ProdutoClick(int codigoUsuario, int codigoProduto) {
        this.codigoProduto = codigoProduto;
        this.codigoUsuario = codigoUsuario;
    }

    public int getCodigoProduto() {
        return codigoProduto;
    }

    public void setCodigoProduto(int codigoProduto) {
        this.codigoProduto = codigoProduto;
    }

    public int getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(int codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }
}

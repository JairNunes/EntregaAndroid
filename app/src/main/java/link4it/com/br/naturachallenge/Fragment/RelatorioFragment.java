package link4it.com.br.naturachallenge.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import link4it.com.br.naturachallenge.MenuActivity;
import link4it.com.br.naturachallenge.R;

/**
 * Created by jair on 19/05/17.
 */

public class RelatorioFragment extends Fragment {

    MenuActivity mContext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_relatorio_consultora, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {

            mContext = (MenuActivity) this.getActivity();


        } catch (Exception err) {
            err.printStackTrace();

        }
    }
}

package support.api.utils;

import android.accounts.NetworkErrorException;
import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import java.io.FileNotFoundException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

public abstract class LogTrace {

    /**
     * Trata o erro, printa o erro no terminal e não exibe alerta
     * @param context
     * @param classe
     * @param err
     * @return
     */
    public static String logCatch(Context context, Class classe, Throwable err)
    {
        return LogTrace.logCatch(context, classe, err, false);
    }

    /**
     * Trata o erro, printa o erro no terminal e exibe alerta amigavel
     * @param context
     * @param classe
     * @param err
     * @param exibeMensagem
     * @return
     */
    public static String logCatch(Context context, Class classe, Throwable err, boolean exibeMensagem)
    {
        StringBuilder sb = new StringBuilder();
        String erro;

        //Verifica qual é a extensão da Exception
        if (err instanceof NetworkErrorException || err instanceof UnknownHostException || err instanceof SocketTimeoutException || err instanceof SocketException)
            erro = "Verifique sua conexão";
        else if (err instanceof FileNotFoundException)
            erro = "Arquivo não encontrado";
        else if (err instanceof SQLiteException)
            erro = "Ocorreu um erro no banco";
        else
            erro = err.getMessage() == null ? "Ocorreu um erro não identificado" : err.getMessage();

        //Monta mensagem de erro
        sb.append(err.getClass().getSimpleName());
        sb.append(" - ");
        sb.append(erro);
        sb.append("!!!");
        erro = sb.toString();

        //ConnectException - Verifique sua conexão!!!
        //NetworkErrorException - Verifique sua conexão!!!

        err.printStackTrace();
        Log.e(classe.getName(), erro);

        if (exibeMensagem) {
            Alerta.show(context, "Erro", erro);
        }

        return erro;
    }

    /**
     * Percorre a exception e retorna uma string com as informações de erro encontradas
     * @param err
     * @return
     */
    public static String getAllException(Throwable err) {
        return LogTrace.getAllException(err, "");
    }
    public static String getAllException(Throwable err, String moreMessage) {
        try {
            StackTraceElement[] arr = err.getStackTrace();
            StringBuilder report = new StringBuilder();

            report.append(err.toString());
            report.append("<br/><br/>");

            report.append("--------- Stack trace ---------");
            report.append("<br/><br/>");
            for (StackTraceElement elem : arr) {
                report.append(String.format("    %1$s", elem.toString()));
                report.append("<br/>");
            }
            report.append("-------------------------------");
            report.append("<br/><br/>");

            // If the exception was thrown in a background thread inside
            // AsyncTask, then the actual exception can be found with getCause
            Throwable cause = err.getCause();
            if (cause != null) {
                report.append("--------- Cause ---------");
                report.append("<br/><br/>");
                report.append(cause.toString());
                report.append("<br/><br/>");
                arr = cause.getStackTrace();
                for (StackTraceElement elem : arr) {
                    report.append(String.format("    %1$s", elem.toString()));
                    report.append("<br/>");
                }
                report.append("-------------------------------");
                report.append("<br/><br/>");
            }

            // Adiciona mais alguma mensagem
            if (!moreMessage.isEmpty()) {
                report.append("--------- More Mensage ---------");
                report.append("<br/><br/>");
                report.append(moreMessage);
                report.append("-------------------------------");
                report.append("<br/><br/>");
            }

            return report.toString();
        }
        catch (Exception e) {
            return e.toString();
        }
    }
}
